import numpy as np
from math import inf


def lawson_hanson(A, b, tol=1e-8, max_iter=10000):

    M = 200
    n = A.shape[1]
    c = np.ones(n)

    # initialize
    n_iter = 0
    x = np.zeros(n)

    active_set = np.ones(n, dtype=np.bool)
    w = A.T.dot(b - A.dot(x)) + M**2*c.dot(1 - c.dot(x))

    while active_set.any() and n_iter < max_iter and w[active_set].max() > tol:
        # if active set emptied: ls solution == nnls solution

        # update sets with minimum index of gradient (max of -grad)
        j = np.argmax(w[active_set])
        active_set[active_set.nonzero()[0][j]] = 0

        # get updated passive_set
        passive_set = ~active_set

        # compute least squares solution A[:, P]y = b
        y = np.zeros(n)

        Ap = A[:, passive_set]
        y[passive_set] = np.linalg.inv(Ap.T.dot(Ap) + M**2*c[passive_set]).dot(Ap.T.dot(b) + M**2*c[passive_set])

        infeasible = (y < tol) & passive_set

        flag = False
        while infeasible.any():
            flag = True
            # find points along line segment xy to backtrack
            alpha = x[infeasible] / (x[infeasible] - y[infeasible])

            # backtrack feasibility vector
            x += alpha.min()*(y - x)

            # move all zero* indices of x from passive to active
            active_set[(np.abs(x) < tol) & passive_set] = 1

            # get updated passive_set
            passive_set = ~active_set

            # compute least squares solution A[:, P]y = b
            y = np.zeros(n)

            Ap = A[:, passive_set]
            y[passive_set] = np.linalg.inv(Ap.T.dot(Ap) + M ** 2 * c[passive_set]).dot(
                Ap.T.dot(b) + M ** 2 * c[passive_set])

            infeasible = (y < tol) & passive_set

        if not flag:
            x = y

        # update iter count
        n_iter += 1

        # compute negative gradient
        w = A.T.dot(b - A.dot(x)) + M ** 2 * c.dot(1 - c.dot(x))

    # return solution
    return x

np.random.seed()

shape = [4, 10]
size = np.prod(shape)

A = np.random.uniform(0, 1, size).reshape(shape)

b = np.random.uniform(0, 1, shape[0])

x = lawson_hanson(A, b)

print(x)

print(b)
print(A.dot(x))